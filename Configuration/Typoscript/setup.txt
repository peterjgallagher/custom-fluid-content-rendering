#custom resources for fluid_styled_content
lib.contentElement{
    templateRootPaths{
        200 = {$custom.templates.templateRootPath}
    }
    partialRootPaths{
        200 = {$custom.templates.partialRootPaths}
    }
    layoutRootPaths{
        200 = {$custom.templates.layoutRootPaths}
    }
}
